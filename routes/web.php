<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});


Route::resource('producto','ProductoController');

Route::get('/exportar', 'ProductoController@exportarProductos');

Route::resource('/login/user', 'CustomLoginController');

//Route::resource('/login', 'CustomLoginController');
Route::post('/login', 'CustomLoginController@loginUser');


Route::resource('/nuevo/usuario', 'UsuarioController');
Route::post('/registrar/usuario', 'UsuarioController@registrarUsuario');

Route::get('/usuarios', 'UsuarioController@allUsers');


