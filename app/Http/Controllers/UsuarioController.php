<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Usuario;

use App\Producto;

use DB;


class UsuarioController extends Controller
{
    //

    public function index(){


        
        
        $usuarios = Usuario::all();
        return view('user.registro', compact('usuarios'));

        //return view('user.registro');

        /*
        $models = Usuario::raw(function($collection)
        {
            $usuarios = $collection->find();
            
            return view('user.registro', compact('usuarios'));
        });
        */
    
    }

    public function allUsers(){

        $usuarios = Usuario::all();

        return response()->json(array('usuarios' => $usuarios));

        /*

        $models = Usuario::raw(function($collection)
        {
            $usuarios = $collection->find();
            
            return view('user.registro', compact('usuarios'));
        });
        */

    }

    public function registrarUsuario(Request $request){

        $rut_user = $request->rut;
        $password_user = $request->password;

        $data = array('rut' => $rut_user, 'password' => $password_user);

        $insertData = DB::connection('mongodb')->collection('user')->insert($data);
        if($insertData){
            return view('user.registro');
        }


        //return view('user.registro');
    
    }
}
