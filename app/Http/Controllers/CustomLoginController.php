<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Usuario;

use App\Producto;

use DB;

class CustomLoginController extends Controller
{
    //

    public function index(){


        return view('user.login');
    
    }

    public function loginUser(Request $request){

        $rut_user = $request->rut;
        $password_user = $request->password;

        $users = Usuario::where('rut', '=', $rut_user)->where('password', '=', $password_user)->count();


        if($users > 0 ){
        
            $producto = Producto::all();

            return view('producto.index', compact('producto'))
            ->with('i', request()->input('page,1'));

        }else{
            return view('user.login');
        }
        

    }
}
