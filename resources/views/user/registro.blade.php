@extends('layout')


@section('content')


    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Nuevo Usuario</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('producto.index') }}"> Volver</a>
            </div>
        </div>
    </div>
   




    @if ($errors->any())
        <div class="alert alert-danger">
            <strong></strong> <br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form>
    	@csrf


         <div class="row">
		    <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>RUT:</strong>
		            <input type="text" name="rut" id="rut" class="form-control" placeholder="RUT">
		        </div>
		    </div>
		    <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Contraseña:</strong>
		            <input type="password" class="form-control" name="password"  id="password" placeholder="Contraseña">
		        </div>
		    </div>
		    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button onclick="RegistrarUsuario();" class="btn btn-primary">Guardar</button>
                
		    </div>
		</div>


    </form>

    <div id="example"></div>

    





    
    


@endsection