<!DOCTYPE html>
<html>
<head>
	<title>Prueba CRUD</title>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/handsontable@7.3.0/dist/handsontable.full.min.js"></script>
	<link href="https://cdn.jsdelivr.net/npm/handsontable@7.3.0/dist/handsontable.full.min.css" rel="stylesheet" media="screen">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body>


<div class="container">
    @yield('content')
</div>


<!-- Login Js!-->
<script type="text/javascript" src="{{ asset('js/registro.js') }}"></script>

</body>
</html>
